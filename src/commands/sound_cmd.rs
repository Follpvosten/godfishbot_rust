use std::path::Path;

use crate::{
    cmd::{Resource::*, *},
    error::*,
};
use rand::{seq::IteratorRandom, thread_rng};
use snafu::ensure;

const SOUND_FILES_DIR: &str = "./sound/";

pub struct SoundCmd {
    name: &'static str,
    descr: Option<&'static str>,
    filenames: Vec<String>,
}

impl SoundCmd {
    pub fn from_file(name: &'static str, filename: &str) -> Result<Self, Error> {
        let filename = SOUND_FILES_DIR.to_string() + filename;
        ensure!(Path::new(&filename).exists(), FileNotFound { filename });
        Ok(SoundCmd {
            name,
            descr: None,
            filenames: vec![filename],
        })
    }
    #[allow(dead_code)]
    pub fn with_descr(mut self, descr: &'static str) -> Self {
        self.descr = Some(descr);
        self
    }
    pub fn another(mut self, filename: &str) -> Result<Self, Error> {
        let filename = SOUND_FILES_DIR.to_string() + filename;
        ensure!(Path::new(&filename).exists(), FileNotFound { filename });
        self.filenames.push(filename);
        Ok(self)
    }
}

impl Cmd for SoundCmd {
    fn name(&self) -> &'static str {
        self.name
    }
    fn usage(&self) -> &'static str {
        self.name
    }
    fn describe(&self) -> &'static str {
        self.descr.unwrap_or("Get a sound effect")
    }
    fn process(&self, _context: &CmdContext, state: Option<&CmdState>) -> CmdResult {
        use CmdState::*;
        match state {
            Some(ResourceID(id)) => CmdResult::voice(TelegramID(id.to_owned())),
            Some(ResourceMap(map)) => {
                let file = self.filenames.iter().choose(&mut thread_rng()).unwrap();
                if let Some(id) = map.get(file) {
                    CmdResult::voice(TelegramID(id.to_owned()))
                } else {
                    CmdResult::voice(Filename(file.clone()))
                }
            }
            None => {
                let res_filename = if self.filenames.len() > 1 {
                    self.filenames.iter().choose(&mut thread_rng())
                } else {
                    self.filenames.get(0)
                };
                match res_filename {
                    Some(filename) => CmdResult::voice(Filename(filename.clone())),
                    None => unreachable!("A SoundCmd without a sound shouldn't exist."),
                }
            }
        }
    }
    fn process_result(
        &self,
        context: &CmdContext,
        prev_result: &ResultData,
        state: Option<&CmdState>,
    ) -> Option<CmdState> {
        use CmdState::*;
        match state {
            Some(ResourceID(_)) => None,
            Some(ResourceMap(m)) => {
                let prev_filename = match prev_result {
                    ResultData::Voice(Filename(name)) => name,
                    _ => return None,
                };
                let file_id = &context.msg.voice.as_ref()?.file_id;
                let mut m = m.clone();
                m.insert(prev_filename.clone(), file_id.clone());
                Some(ResourceMap(m))
            }
            None => {
                let file_id = &context.msg.voice.as_ref()?.file_id;
                if self.filenames.len() > 1 {
                    let prev_filename = match prev_result {
                        ResultData::Voice(Filename(name)) => name,
                        _ => return None,
                    };
                    let entry = [(prev_filename.clone(), file_id.clone())];
                    Some(ResourceMap(entry.iter().cloned().collect()))
                } else {
                    Some(ResourceID(file_id.clone()))
                }
            }
        }
    }
}
