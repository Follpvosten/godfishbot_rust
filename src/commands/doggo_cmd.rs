use failure::Error;
use futures::Future;
use serde::Deserialize;

use crate::cmd::*;

pub struct DoggoCmd {
    all_breeds: Vec<String>,
}

#[derive(Deserialize)]
struct DoggoQueryResult {
    message: String,
    status: String,
}

impl DoggoCmd {
    pub fn new() -> Self {
        let all_breeds = Self::load_breeds().unwrap_or_else(|err| {
            eprintln!("Error loading breeds: {}", err);
            eprintln!("Continuing without breed list...");
            vec![]
        });
        Self { all_breeds }
    }
    fn load_breeds() -> Result<Vec<String>, Box<dyn std::error::Error>> {
        let result: serde_json::Value =
            reqwest::get("https://dog.ceo/api/breeds/list/all")?.json()?;
        if result.get("status").unwrap().as_str().unwrap() != "success" {
            return Err(result.get("message").unwrap().as_str().unwrap().into());
        }
        let breeds: std::collections::HashMap<String, Vec<String>> =
            serde_json::from_value(result.get("message").unwrap().to_owned())?;
        Ok(breeds
            .into_iter()
            .flat_map(|(breed, sub_breeds)| {
                if !sub_breeds.is_empty() {
                    sub_breeds
                        .iter()
                        .map(|sb| format!("{} {}", sb, breed.clone()))
                        .collect::<Vec<_>>()
                        .into_iter()
                } else {
                    vec![breed].into_iter()
                }
            })
            .collect())
    }

    fn query_api(
        &self,
        breed: Option<String>,
    ) -> impl Future<Item = ResultData, Error = Error> + '_ {
        use reqwest::r#async::Client;

        let url = if let Some(breed) = &breed {
            use std::borrow::Cow;
            let breed = if breed.contains(' ') {
                Cow::Owned(breed.rsplit(' ').collect::<Vec<_>>().join("/"))
            } else {
                Cow::Borrowed(breed)
            };
            format!(
                "https://dog.ceo/api/breed/{}/images/random",
                breed.to_ascii_lowercase()
            )
        } else {
            "https://dog.ceo/api/breeds/image/random".into()
        };

        Client::new()
            .get(&url)
            .send()
            .and_then(|mut res| res.json::<DoggoQueryResult>())
            .and_then(move |result| {
                use Resource::*;
                use ResultData::*;
                Ok(match result.status.as_ref() {
                    "success" => Image(Url(result.message), None),
                    _ => {
                        if let Some(breed) = breed {
                            let breed_results: Vec<&String> = self
                                .all_breeds
                                .iter()
                                .filter(|b| b.contains(&breed.to_ascii_lowercase()))
                                .collect();
                            if !breed_results.is_empty() {
                                let result_texts = breed_results
                                    .into_iter()
                                    .map(|v| v.to_owned())
                                    .collect::<Vec<_>>()
                                    .join("\n");
                                Text(format!("Did you mean any of these:\n{}", result_texts))
                            } else {
                                Text("Breed not found!".into())
                            }
                        } else {
                            Text(result.message)
                        }
                    }
                })
            })
            .map_err(Error::from)
    }
}

impl Cmd for DoggoCmd {
    fn name(&self) -> &'static str {
        "/doggo"
    }
    fn usage(&self) -> &'static str {
        "/doggo [breed]"
    }
    fn describe(&self) -> &'static str {
        "Get a random doggo from teh interwebs (alternatively by breed)"
    }
    fn process(&self, context: &CmdContext, _state: Option<&CmdState>) -> CmdResult {
        CmdResult::future(Box::new(self.query_api(context.args.clone())))
    }
}
