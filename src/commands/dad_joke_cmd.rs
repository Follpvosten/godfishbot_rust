use failure::Error;
use futures::Future;

use crate::cmd::*;

pub struct DadJokeCmd {
    name: &'static str,
}

impl DadJokeCmd {
    #[allow(dead_code)]
    pub fn new() -> Self {
        Self { name: "/dadjoke" }
    }
    pub fn with_name(name: &'static str) -> Self {
        Self { name }
    }
}

impl Cmd for DadJokeCmd {
    fn name(&self) -> &'static str {
        self.name
    }
    fn usage(&self) -> &'static str {
        self.name
    }
    fn describe(&self) -> &'static str {
        "Get a random dad joke from https://icanhazdadjoke.com/api"
    }
    fn process(&self, _context: &CmdContext, _state: Option<&CmdState>) -> CmdResult {
        use reqwest::r#async::Client;
        let f = Client::new()
            .get("https://icanhazdadjoke.com/")
            .header("Accept", "application/json")
            .header(
                "User-Agent",
                "godfishbot_rust (https://gitlab.com/Follpvosten/godfishbot_rust)",
            )
            .send()
            .and_then(|mut res| res.json::<serde_json::Value>())
            .and_then(move |json| {
                Ok(match json.get("joke") {
                    Some(f) => match f.as_str() {
                        Some(s) => s.to_owned(),
                        None => "Failed to get string: joke".to_owned(),
                    },
                    None => "json_field not found: joke".to_owned(),
                })
            })
            .map_err(Error::from)
            .map(ResultData::Text);
        CmdResult::future(Box::new(f))
    }
}
