use snafu::Snafu;
use std::io;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub(crate)")]
pub enum Error {
    #[snafu(display("File not found: {}", filename))]
    FileNotFound { filename: String },
    #[snafu(display("Error reading path {}: {}", path, source))]
    ReadDir { path: String, source: io::Error },
    #[snafu(display("Error reading file {}: {}", path, source))]
    ReadFile { path: String, source: io::Error },
}
