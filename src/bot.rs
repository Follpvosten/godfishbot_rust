use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
};

use futures::{
    future::{Either, Future, IntoFuture},
    stream::Stream,
};
use telebot::{bot::RequestHandle, file::File, functions::*, objects::Update};

use crate::cmd::*;

pub struct GodfishBot {
    inner: telebot::Bot,
    cmds: HashMap<&'static str, Box<dyn Cmd>>,
    states: HashMap<&'static str, Arc<RwLock<Option<CmdState>>>>,
}

impl GodfishBot {
    fn update_to_request(
        &self,
        bot: RequestHandle,
        mut update: Update,
    ) -> Option<(CmdContext, &Box<dyn Cmd>)> {
        let msg = update.message.take()?;
        let (user, text) = (msg.from.clone()?, msg.text.clone()?);
        let first_word = {
            let first_word = text.split_whitespace().nth(0)?;
            if first_word.contains('@') {
                first_word.split('@').nth(0)?
            } else {
                first_word
            }
        };
        let cmd = self.cmds.get(first_word)?;
        let args = &text[first_word.len()..].trim();
        let args = if !args.is_empty() {
            Some((*args).to_string())
        } else {
            None
        };
        let context = CmdContext {
            bot,
            msg,
            user,
            args,
        };
        Some((context, cmd))
    }
    fn process_request<'a>(
        &'a self,
        context: CmdContext,
        cmd: &'a Box<dyn Cmd>,
    ) -> impl Future<
        Item = (CmdContext, &'a Box<dyn Cmd>, ReplyTarget, ResultData),
        Error = failure::Error,
    > + 'a {
        use futures::future::result;
        use ResultDataOrFuture::*;
        let cmd_result = {
            let state_arc = Arc::clone(&self.states[cmd.name()]);
            let state = state_arc.read().unwrap();
            cmd.process(&context, state.as_ref())
        };
        let reply_to = cmd_result.reply_to;
        match cmd_result.data {
            Future(f) => Either::A(
                // Map internal errors to Text results so they get reported to the user
                f.or_else(|err| Ok(crate::cmd::ResultData::Text(err.to_string())))
                    .map(move |data| (context, cmd, reply_to, data)),
            ),
            ResultData(data) => Either::B(result(Ok((context, cmd, reply_to, data)))),
        }
    }
    fn send_result<'a>(
        &'a self,
        context: CmdContext,
        cmd: &'a Box<dyn Cmd>,
        reply: ReplyTarget,
        result_data: ResultData,
    ) -> impl Future<Item = (CmdContext, &'a Box<dyn Cmd>, ResultData), Error = failure::Error> + 'a
    {
        match &result_data {
            ResultData::Text(txt) => {
                let mut send_msg = context
                    .bot
                    .message(context.msg.chat.id, txt.clone())
                    .disable_web_page_preview(true);
                match reply {
                    ReplyTarget::None => {}
                    ReplyTarget::CommandSender => {
                        send_msg = send_msg.reply_to_message_id(context.msg.message_id)
                    }
                    ReplyTarget::Other(id) => send_msg = send_msg.reply_to_message_id(id),
                };
                Either::A(send_msg.send())
            }
            ResultData::Image(res, caption) => {
                use Resource::*;
                let mut send_img = context.bot.photo(context.msg.chat.id).file(match res {
                    Url(url) => File::Url(url.clone()),
                    Filename(file) => File::Disk { path: file.into() },
                    TelegramID(res_id) => File::Telegram(res_id.clone()),
                });
                if let Some(text) = caption {
                    send_img = send_img.caption(text);
                }
                match reply {
                    ReplyTarget::None => {}
                    ReplyTarget::CommandSender => {
                        send_img = send_img.reply_to_message_id(context.msg.message_id)
                    }
                    ReplyTarget::Other(id) => send_img = send_img.reply_to_message_id(id),
                };
                Either::B(Either::A(send_img.send()))
            }
            ResultData::Voice(res) => {
                use Resource::*;
                let mut send_audio = context.bot.voice(context.msg.chat.id).file(match res {
                    Url(url) => File::Url(url.clone()),
                    Filename(file) => File::Disk { path: file.into() },
                    TelegramID(res_id) => File::Telegram(res_id.clone()),
                });
                match reply {
                    ReplyTarget::None => {}
                    ReplyTarget::CommandSender => {
                        send_audio = send_audio.reply_to_message_id(context.msg.message_id)
                    }
                    ReplyTarget::Other(id) => send_audio = send_audio.reply_to_message_id(id),
                };
                Either::B(Either::B(send_audio.send()))
            }
        }
        .map(move |(_bot, msg)| (CmdContext { msg, ..context }, cmd, result_data))
    }
    fn process_result<'a>(
        &'a self,
        context: CmdContext,
        cmd: &'a Box<dyn Cmd>,
        result_data: ResultData,
    ) -> impl IntoFuture<Error = failure::Error> + 'a {
        let state_arc = Arc::clone(&self.states[cmd.name()]);
        let res = {
            let state = state_arc.read().unwrap();
            cmd.process_result(&context, &result_data, state.as_ref())
        };
        if let Some(new_state) = res {
            (state_arc.write().unwrap()).replace(new_state);
        }
        Ok(())
    }

    pub fn run(&self, core: &mut tokio_core::reactor::Core) -> Result<(), failure::Error> {
        let stream = self
            .inner
            .clone()
            .get_stream(None)
            .filter_map(|(handle, update)| self.update_to_request(handle, update))
            .and_then(|(context, cmd)| self.process_request(context, cmd))
            .and_then(|(context, cmd, reply, result_data)| {
                self.send_result(context, cmd, reply, result_data)
            })
            .and_then(|(context, cmd, result_data)| self.process_result(context, cmd, result_data))
            .for_each(|_| Ok(()));
        core.run(stream.into_future())
    }
    pub fn builder(token: String) -> GodfishBotBuilder {
        GodfishBotBuilder::new(token)
    }
}

pub struct GodfishBotBuilder {
    token: String,
    cmds: Vec<Box<dyn Cmd>>,
}

impl GodfishBotBuilder {
    pub fn new(token: String) -> Self {
        Self {
            token,
            cmds: Vec::new(),
        }
    }
    pub fn cmd<T: Cmd + 'static>(mut self, cmd: T) -> Self {
        self.cmds.push(Box::new(cmd));
        self
    }
    pub fn build(mut self) -> GodfishBot {
        let inner = telebot::Bot::new(&self.token).update_interval(200);
        let help_cmd = crate::commands::HelpCmd::from_commands(&self.cmds);
        self.cmds.push(Box::new(help_cmd));
        let cmds: HashMap<&'static str, Box<dyn Cmd>> =
            self.cmds.into_iter().map(|c| (c.name(), c)).collect();
        let states = cmds
            .iter()
            .map(|(n, _)| (*n, Arc::new(RwLock::new(None))))
            .collect();
        GodfishBot {
            inner,
            cmds,
            states,
        }
    }
}
