use std::{collections::HashMap, fs};

use crate::{
    cmd::{Resource::*, ResultData::*, *},
    error::{Error, ReadDir},
};
use rand::{seq::IteratorRandom, thread_rng};
use snafu::ResultExt;

pub struct RandImgCmd {
    name: &'static str,
    descr: &'static str,
    options: Vec<String>,
}

impl RandImgCmd {
    pub fn from_folder(name: &'static str, descr: &'static str, path: &str) -> Result<Self, Error> {
        let paths = fs::read_dir(path)
            .context(ReadDir { path })?
            .collect::<Vec<_>>();
        let mut options = Vec::with_capacity(paths.len());
        for filepath in paths {
            options.push(
                filepath
                    .context(ReadDir { path })?
                    .path()
                    .display()
                    .to_string(),
            );
        }
        Ok(RandImgCmd {
            name,
            descr,
            options,
        })
    }
}

impl Cmd for RandImgCmd {
    fn name(&self) -> &'static str {
        self.name
    }
    fn describe(&self) -> &'static str {
        self.descr
    }
    fn usage(&self) -> &'static str {
        self.name
    }
    fn process(&self, _context: &CmdContext, state: Option<&CmdState>) -> CmdResult {
        if let Some(path) = self.options.iter().choose(&mut thread_rng()) {
            if let Some(state) = state.map(|s| match s {
                CmdState::ResourceID(_) => unreachable!(),
                CmdState::ResourceMap(rm) => rm,
            }) {
                if let Some(res_id) = state.get(path) {
                    CmdResult::img(TelegramID(res_id.clone()))
                } else {
                    CmdResult::img(Filename(path.clone()))
                }
            } else {
                CmdResult::img(Filename(path.clone()))
            }
        } else {
            CmdResult::text("No images provided!").reply(true)
        }
    }
    fn process_result(
        &self,
        context: &CmdContext,
        prev_result: &ResultData,
        state: Option<&CmdState>,
    ) -> Option<CmdState> {
        let prev_filename = match prev_result {
            Image(res, _) => match res {
                TelegramID(_) => return None,
                Filename(name) => name,
                Url(url) => url,
            },
            _ => return None,
        };
        let file_id = &context.msg.photo.as_ref()?.first()?.file_id;
        let mut curr_state = state
            .map(|s| match s {
                CmdState::ResourceID(_) => unreachable!(),
                CmdState::ResourceMap(rm) => rm.clone(),
            })
            .unwrap_or_else(HashMap::new);
        curr_state.insert(prev_filename.clone(), file_id.clone());
        Some(CmdState::ResourceMap(curr_state))
    }
}
