use failure::Error;
use futures::future::Future;
use std::collections::HashMap;
use telebot::{
    bot::RequestHandle,
    objects::{Message, User},
};

pub struct CmdContext {
    pub bot: RequestHandle,
    pub msg: Message,
    pub user: User,
    pub args: Option<String>,
}
#[derive(Clone)]
pub enum CmdState {
    ResourceID(String),
    ResourceMap(HashMap<String, String>),
}

pub enum ResultData {
    Text(String),
    Image(Resource, Option<String>),
    Voice(Resource),
}
pub enum ResultDataOrFuture<'a> {
    ResultData(ResultData),
    Future(Box<dyn Future<Item = ResultData, Error = Error> + 'a>),
}
impl<'a> From<ResultData> for ResultDataOrFuture<'a> {
    fn from(rd: ResultData) -> Self {
        ResultDataOrFuture::ResultData(rd)
    }
}
pub enum ReplyTarget {
    None,
    CommandSender,
    Other(i64),
}
pub struct CmdResult<'a> {
    pub data: ResultDataOrFuture<'a>,
    pub reply_to: ReplyTarget,
}
impl<'a> CmdResult<'a> {
    pub fn new(data: ResultDataOrFuture<'a>) -> Self {
        CmdResult {
            data,
            reply_to: ReplyTarget::None,
        }
    }
    pub fn text<T: Into<String>>(text: T) -> Self {
        Self::new(ResultData::Text(text.into()).into())
    }
    pub fn img(res: Resource) -> Self {
        Self::new(ResultData::Image(res, None).into())
    }
    pub fn voice(res: Resource) -> Self {
        Self::new(ResultData::Voice(res).into())
    }
    pub fn future(fut: Box<dyn Future<Item = ResultData, Error = Error> + 'a>) -> Self {
        Self::new(ResultDataOrFuture::Future(fut))
    }
    pub fn reply(mut self, reply: bool) -> Self {
        self.reply_to = if reply {
            ReplyTarget::CommandSender
        } else {
            ReplyTarget::None
        };
        self
    }
    pub fn reply_to(mut self, reply_to_id: i64) -> Self {
        self.reply_to = ReplyTarget::Other(reply_to_id);
        self
    }
    #[allow(dead_code)]
    pub fn caption<T: Into<String> + 'a>(mut self, caption: T) -> Self {
        use self::ResultData::*;
        use ResultDataOrFuture::*;
        self.data = match self.data {
            ResultData(data) => ResultData(match data {
                Text(_) => Text(caption.into()),
                Image(res, _) => Image(res, Some(caption.into())),
                Voice(v) => Voice(v),
            }),
            Future(f) => Future(Box::new(f.map(|data| match data {
                Text(_) => Text(caption.into()),
                Image(res, _) => Image(res, Some(caption.into())),
                Voice(v) => Voice(v),
            }))),
        };
        self
    }
}

pub trait Cmd {
    fn name(&self) -> &'static str;
    fn usage(&self) -> &'static str;
    fn describe(&self) -> &'static str;
    fn process(&self, context: &CmdContext, state: Option<&CmdState>) -> CmdResult;
    #[allow(unused_variables)]
    fn process_result(
        &self,
        context: &CmdContext,
        prev_result: &ResultData,
        state: Option<&CmdState>,
    ) -> Option<CmdState> {
        None
    }
}

pub enum Resource {
    Url(String),
    Filename(String),
    TelegramID(String),
}
