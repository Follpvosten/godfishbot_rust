use std::collections::{BTreeMap, HashMap};

use crate::cmd::*;

pub struct TestLoveCmd;

const LOVE_VAL: &str = "ILOVE";

impl TestLoveCmd {
    fn get_count(name1: &str, name2: &str) -> Vec<usize> {
        let mut map = BTreeMap::new();
        let names = name1.to_ascii_uppercase() + &name2.to_ascii_uppercase() + LOVE_VAL;
        for ch in names.chars() {
            if let Some(val) = map.get_mut(&ch) {
                *val += 1;
            } else {
                map.insert(ch, 1);
            }
        }
        map.values().copied().collect()
    }

    fn test_love(name1: &str, name2: &str) -> String {
        let (name1, name2) = if name1 > name2 {
            (name1, name2)
        } else {
            (name2, name1)
        };
        let mut count = Self::get_count(name1, name2);
        if count.len() == 1 {
            return count[0].to_string();
        } else if count.len() == 2 {
            return count[0].to_string() + &count[1].to_string();
        }
        while count.len() != 2 {
            let mut sub: Vec<usize> = Vec::new();
            let size = count.len() / 2;
            for i in 0..size {
                let new_c = (count[i] + count[count.len() - 1 - i]).to_string();
                new_c
                    .chars()
                    .filter_map(|c| c.to_string().parse().ok())
                    .for_each(|c| sub.push(c));
            }
            if count.len() != size * 2 {
                sub.push(count[size]);
            }
            count = sub;
        }
        count[0].to_string() + &count[1].to_string()
    }

    fn rank_love(names: &[&str]) -> String {
        let mut combos = HashMap::new();
        for name1 in names.iter() {
            for name2 in names.iter() {
                if combos.contains_key(&(*name1, *name2))
                    || combos.contains_key(&(*name2, *name1))
                    || *name1 == *name2
                {
                    continue;
                }
                combos.insert((*name1, *name2), Self::test_love(*name1, *name2));
            }
        }
        let mut tmp = combos.iter().collect::<Vec<_>>();
        tmp.sort_by(|t1, t2| t2.1.partial_cmp(t1.1).unwrap());
        tmp.iter()
            .enumerate()
            .map(|(i, ((name1, name2), result))| {
                format!("{}. {} x {} ({}%)", i + 1, name1, name2, result)
            })
            .collect::<Vec<_>>()
            .join("\n")
    }
}

impl Cmd for TestLoveCmd {
    fn name(&self) -> &'static str {
        "/testlove"
    }
    fn usage(&self) -> &'static str {
        "/testlove <list of names>"
    }
    fn describe(&self) -> &'static str {
        "Test compatibility based on names. Totally scientifically correct!"
    }
    fn process(&self, context: &CmdContext, _state: Option<&CmdState>) -> CmdResult {
        if let Some(args) = &context.args {
            let names: Vec<&str> = if args.contains('\n') {
                args.split('\n').collect()
            } else {
                args.split(' ').collect()
            };
            if names.len() < 2 {
                return CmdResult::text("Please submit at least two names.");
            }
            let result = if names.len() > 2 {
                Self::rank_love(&names)
            } else {
                let str_result = Self::test_love(names[0], names[1]);
                format!("{} and {} fit {}%.", names[0], names[1], str_result)
            };
            CmdResult::text(result)
        } else {
            CmdResult::text(format!("Usage: {}", self.usage()))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::TestLoveCmd;
    const NAMES: &[&str] = &["Madame Carmalita", "Tante Margaritha", "Señor Casablanca"];

    #[test]
    fn consistent_count() {
        for _ in 0..5 {
            let (result1, result2) = (
                TestLoveCmd::get_count(NAMES[0], NAMES[1]),
                TestLoveCmd::get_count(NAMES[0], NAMES[1]),
            );
            assert_eq!(result1, result2);
        }
    }
    #[test]
    fn consistent_love() {
        for _ in 0..5 {
            let (result1, result2) = (
                TestLoveCmd::test_love(NAMES[0], NAMES[1]),
                TestLoveCmd::test_love(NAMES[0], NAMES[1]),
            );
            assert_eq!(result1, result2);
        }
    }
    #[test]
    fn consistent_rank() {
        for _ in 0..5 {
            let (result1, result2) = (TestLoveCmd::rank_love(NAMES), TestLoveCmd::rank_love(NAMES));
            assert_eq!(result1, result2);
        }
    }
}
