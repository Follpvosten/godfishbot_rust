use crate::{cmd::*, error::*};
use rand::{seq::IteratorRandom, thread_rng};
use snafu::ResultExt;

pub struct RandTextCmd {
    name: &'static str,
    descr: &'static str,
    usage: &'static str,
    options: Vec<String>,
    options_single: Option<Vec<String>>,
}

impl RandTextCmd {
    pub fn from_file(
        name: &'static str,
        usage: &'static str,
        descr: &'static str,
        opts_file: &str,
    ) -> Result<RandTextCmd, Error> {
        Ok(RandTextCmd {
            name,
            usage,
            descr,
            options: Self::get_file_lines(opts_file).context(ReadFile { path: opts_file })?,
            options_single: None,
        })
    }
    pub fn with_single_file(mut self, filename: &str) -> Result<RandTextCmd, Error> {
        self.options_single =
            Some(Self::get_file_lines(filename).context(ReadFile { path: filename })?);
        Ok(self)
    }
    fn get_file_lines(filename: &str) -> std::io::Result<Vec<String>> {
        Ok(std::fs::read_to_string(filename)?
            .lines()
            .map(ToOwned::to_owned)
            .collect())
    }

    fn random_sentence(&self, username: &str) -> String {
        if let Some(options) = &self.options_single {
            options
                .iter()
                .choose(&mut thread_rng())
                .expect(&("No options on ".to_owned() + self.name))
                .replace("{0}", username)
        } else {
            self.random_sentence_at(username, "Baumhardt")
        }
    }
    fn random_sentence_at(&self, username: &str, target: &str) -> String {
        self.options
            .iter()
            .choose(&mut thread_rng())
            .expect(&("No options on ".to_owned() + self.name))
            .replace("{0}", username)
            .replace("{1}", target)
    }
}

impl Cmd for RandTextCmd {
    fn name(&self) -> &'static str {
        self.name
    }
    fn usage(&self) -> &'static str {
        self.usage
    }
    fn describe(&self) -> &'static str {
        self.descr
    }
    fn process(&self, context: &CmdContext, _state: Option<&CmdState>) -> CmdResult {
        let text = if let Some(args) = &context.args {
            self.random_sentence_at(&context.user.first_name, &args)
        } else if self.options_single.is_some() {
            self.random_sentence(&context.user.first_name)
        } else {
            "Usage: ".to_owned() + self.usage
        };
        CmdResult::text(text)
    }
}
