use crate::cmd::*;
use std::collections::HashMap;

pub struct HelpCmd {
    text: String,
    help_texts: HashMap<String, String>,
}

impl<'a> HelpCmd {
    pub fn from_commands(cmds: &[Box<dyn Cmd>]) -> Self {
        let text = cmds
            .iter()
            .map(|c| c.usage().to_owned())
            .collect::<Vec<String>>()
            .join("\n");
        let text = "Available commands:\n\n".to_owned() + &text;
        let help_texts: HashMap<String, String> = cmds
            .iter()
            .map(|c| {
                (
                    c.name()[1..].to_owned(),
                    "Usage: ".to_owned() + c.usage() + "\n\n" + c.describe(),
                )
            })
            .collect();
        HelpCmd { text, help_texts }
    }
}

impl Cmd for HelpCmd {
    fn name(&self) -> &'static str {
        "/help"
    }
    fn usage(&self) -> &'static str {
        "/help"
    }
    fn describe(&self) -> &'static str {
        "Offers help for commands (or a list of commands)"
    }
    fn process(&self, context: &CmdContext, _state: Option<&CmdState>) -> CmdResult {
        let result_text = if let Some(args) = &context.args {
            if let Some(help) = self.help_texts.get(args) {
                &help
            } else {
                "Command not found!"
            }
        } else {
            &self.text
        };
        CmdResult::text(result_text).reply(true)
    }
}
