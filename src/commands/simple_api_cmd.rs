use failure::Error;
use futures::Future;

use crate::cmd::*;

pub struct SimpleApiCmd {
    name: &'static str,
    descr: Option<&'static str>,
    url: String,
    json_field: String,
    outer_json_field: Option<String>,
}

impl SimpleApiCmd {
    pub fn new<T: Into<String>>(name: &'static str, url: T, json_field: T) -> Self {
        SimpleApiCmd {
            name,
            descr: None,
            url: url.into(),
            json_field: json_field.into(),
            outer_json_field: None,
        }
    }
    pub fn with_descr(mut self, descr: &'static str) -> Self {
        self.descr = Some(descr);
        self
    }
    pub fn with_outer_field<T: Into<String>>(mut self, field: T) -> Self {
        self.outer_json_field = Some(field.into());
        self
    }
    fn query_api(&self) -> impl Future<Item = ResultData, Error = Error> + '_ {
        use reqwest::r#async::Client;

        Client::new()
            .get(&self.url)
            .send()
            .and_then(|mut res| res.json::<serde_json::Value>())
            .and_then(move |mut json| {
                if let Some(outer_field) = &self.outer_json_field {
                    json = match json.get(outer_field) {
                        Some(f) => f.to_owned(),
                        None => {
                            return Ok(
                                format!("outer_json_field not found: {}", outer_field)
                            )
                        }
                    };
                }
                Ok(match json.get(&self.json_field) {
                    Some(f) => match f.as_str() {
                        Some(s) => s.to_owned(),
                        None => format!("Failed to get string: {}", self.json_field),
                    },
                    None => format!("json_field not found: {}", self.json_field),
                })
            })
            .map_err(Error::from)
            .map(ResultData::Text)
    }
}

impl Cmd for SimpleApiCmd {
    fn name(&self) -> &'static str {
        self.name
    }
    fn usage(&self) -> &'static str {
        self.name
    }
    fn describe(&self) -> &'static str {
        self.descr.unwrap_or("Access some API, I guess?")
    }
    fn process(&self, _context: &CmdContext, _state: Option<&CmdState>) -> CmdResult {
        CmdResult::future(Box::new(self.query_api()))
    }
}
