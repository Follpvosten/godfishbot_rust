use std::fs;
use tokio_core::reactor::Core;

mod config;
use config::*;
mod error;

mod cmd;
mod commands;
use commands::*;

mod bot;
use bot::*;

fn main() -> Result<(), error::Error> {
    println!("Starting godfishbot...");
    let mut lp = Core::new().unwrap();

    println!("Loading config file BotConfig.toml...");
    let bot_config: Config = fs::read_to_string("BotConfig.toml")
        .expect("Failed to read config file!")
        .parse()
        .expect("Failed to parse config!");

    println!("Setting up commands...");
    let bot = GodfishBot::builder(bot_config.token)
        .cmd(
            RandTextCmd::from_file(
                "/explode",
                "/explode [target]",
                "Explode (at someone, optionally)",
                "explode.txt",
            )?
            .with_single_file("explode_single.txt")?,
        )
        .cmd(RandTextCmd::from_file(
            "/kiss",
            "/kiss <target>",
            "Kiss someone",
            "kiss.txt",
        )?)
        .cmd(RandTextCmd::from_file(
            "/hug",
            "/hug <target>",
            "Hug someone",
            "hugs.txt",
        )?)
        .cmd(RandImgCmd::from_folder("/star", "Get a star", "./stars/")?)
        .cmd(SoundCmd::from_file("/bitchwhere", "bitchwhere.mp3")?)
        .cmd(SoundCmd::from_file("/boahey", "boahey.ogg")?)
        .cmd(SoundCmd::from_file("/eeyup", "eeyup.opus")?)
        .cmd(SoundCmd::from_file("/eghugh", "eghughehhhh.mp3")?)
        .cmd(SoundCmd::from_file("/arsam", "failure.mp3")?.with_descr("YOU FUCKING FAILURE!"))
        .cmd(SoundCmd::from_file("/gasp", "gasp.opus")?)
        .cmd(SoundCmd::from_file("/heuldoch", "heuldoch.ogg")?)
        .cmd(SoundCmd::from_file("/okay", "okay.mp3")?)
        .cmd(SoundCmd::from_file("/truthahn", "truthahn.ogg")?)
        .cmd(SoundCmd::from_file("/ululu", "ululu.opus")?)
        .cmd(SoundCmd::from_file("/property", "property.mp3")?.another("property2.mp3")?)
        .cmd(SoundCmd::from_file("/sixpack", "sixpack.mp3")?)
        .cmd(SoundCmd::from_file("/sexy", "sexy.mp3")?)
        .cmd(SoundCmd::from_file("/ayaya", "ayaya1.mp3")?.another("ayaya2.mp3")?)
        .cmd(SoundCmd::from_file("/nigerundayo", "nigerundayo.mp3")?)
        .cmd(
            SoundCmd::from_file("/wow", "wow.mp3")?
                .another("wow2.mp3")?
                .another("wow3.mp3")?,
        )
        .cmd(SoundCmd::from_file("/nneville", "nneville.mp3")?)
        .cmd(SoundCmd::from_file("/saido", "saidochesto.mp3")?)
        .cmd(SoundCmd::from_file("/ohyeah", "ohyeah1.mp3")?.another("ohyeah2.mp3")?)
        .cmd(SoundCmd::from_file("/damedame", "damedame.mp3")?)
        .cmd(SoundCmd::from_file("/yeah", "yeah.mp3")?)
        .cmd(SoundCmd::from_file("/dingdong", "dingdong.mp3")?)
        .cmd(SoundCmd::from_file("/horn", "horn.mp3")?)
        .cmd(SoundCmd::from_file("/nani", "nani.mp3")?)
        .cmd(SoundCmd::from_file("/explosion", "explosion1.mp3")?.another("explosion2.mp3")?)
        .cmd(SoundCmd::from_file("/french", "french.mp3")?)
        .cmd(SoundCmd::from_file("/chinese", "chinese.mp3")?)
        .cmd(SoundCmd::from_file("/friendship", "friendship.mp3")?)
        .cmd(SoundCmd::from_file("/selfie", "selfie.mp3")?)
        .cmd(SoundCmd::from_file("/baum", "baum.mp3")?)
        .cmd(SoundCmd::from_file("/dundundun", "dundundun.mp3")?)
        .cmd(SoundCmd::from_file("/sasgay", "sasuke.mp3")?)
        .cmd(SoundCmd::from_file("/naruto", "naruto.mp3")?)
        .cmd(SoundCmd::from_file("/alpakistan", "oreimo.mp3")?)
        .cmd(SoundCmd::from_file("/pling", "pling.mp3")?)
        .cmd(SoundCmd::from_file("/laugh", "laugh.mp3")?)
        .cmd(SoundCmd::from_file("/power", "woahohohah.mp3")?)
        .cmd(SoundCmd::from_file("/zawarudo", "zawarudo.mp3")?)
        .cmd(SoundCmd::from_file("/wah", "wah.mp3")?)
        .cmd(SoundCmd::from_file("/checkmate", "checkometo.mp3")?)
        .cmd(SoundCmd::from_file("/nintendo", "daisy.mp3")?)
        .cmd(SoundCmd::from_file("/heal", "heal.mp3")?)
        .cmd(SoundCmd::from_file("/mammamia", "mammamia.mp3")?)
        .cmd(SoundCmd::from_file("/morioh", "morioh.mp3")?)
        .cmd(SoundCmd::from_file("/youready", "youready.mp3")?)
        .cmd(SoundCmd::from_file("/herewego", "herewego.mp3")?)
        .cmd(SoundCmd::from_file("/again", "again.mp3")?)
        .cmd(SoundCmd::from_file("/uuuh", "uuuh.mp3")?)
        .cmd(SoundCmd::from_file("/fbi", "fbi.mp3")?)
        .cmd(SoundCmd::from_file("/rivalun", "rivalun.mp3")?)
        .cmd(SoundCmd::from_file("/iamconfusion", "iamconfusion.mp3")?)
        .cmd(ImgCmd::from_file("/bully", "bully.jpg")?.reply())
        .cmd(ImgCmd::from_file("/bully2", "bully2.jpg")?.reply())
        .cmd(ImgCmd::from_file("/spicken", "spicken.jpg")?.reply())
        .cmd(ImgCmd::from_file("/frenz", "frenz.jpg")?.reply())
        .cmd(ImgCmd::from_file("/teacher", "teacher.jpg")?.reply())
        .cmd(ImgCmd::from_file("/bullyback", "bullyback.jpg")?.reply())
        .cmd(ImgCmd::from_file("/tease", "tease.jpg")?.reply())
        .cmd(ImgCmd::from_file("/flashbacks", "flashback.jpg")?)
        .cmd(
            SimpleApiCmd::new(
                "/cn",
                "http://api.icndb.com/jokes/random?escape=javascript",
                "joke",
            )
            .with_descr("Get a fact about Chuck Norris. (Powered by http://www.icndb.com)")
            .with_outer_field("value"),
        )
        .cmd(
            SimpleApiCmd::new(
                "/trump",
                "https://api.whatdoestrumpthink.com/api/v1/quotes/random",
                "message",
            )
            .with_descr("Get a Donald Trump quote. Powered by https://whatdoestrumpthink.com"),
        )
        .cmd(DadJokeCmd::with_name("/dadjoke"))
        .cmd(SimpleApiCmd::new(
            "/catfact",
            "https://cat-fact.herokuapp.com/facts/random",
            "text",
        ))
        .cmd(SimpleApiCmd::new(
            "/funfact",
            "https://uselessfacts.jsph.pl/random.json?language=en",
            "text",
        ))
        .cmd(DoggoCmd::new())
        .cmd(TestLoveCmd)
        .build();

    println!("Launching the bot...");
    // Should never return Ok
    while let Err(e) = bot.run(&mut lp) {
        eprintln!("Event loop shutdown:");
        for (i, cause) in e.iter_causes().enumerate() {
            eprintln!(" => {}: {}", i, cause);
        }
    }
    Ok(())
}
