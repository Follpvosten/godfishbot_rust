use serde_derive::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub token: String,
}
impl std::str::FromStr for Config {
    type Err = toml::de::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        toml::from_str(s)
    }
}
