use std::path::Path;

use crate::{
    cmd::{Resource::*, *},
    error::*,
};
use snafu::ensure;

const IMG_FILES_DIR: &str = "./images/";

pub struct ImgCmd {
    name: &'static str,
    descr: Option<&'static str>,
    reply: bool,
    filename: String,
}

impl ImgCmd {
    pub fn from_file(name: &'static str, filename: &str) -> Result<Self, Error> {
        let filename = IMG_FILES_DIR.to_owned() + filename;
        ensure!(Path::new(&filename).exists(), FileNotFound { filename });
        Ok(ImgCmd {
            name,
            descr: None,
            reply: false,
            filename,
        })
    }
    pub fn reply(mut self) -> Self {
        self.reply = true;
        self
    }
}

impl Cmd for ImgCmd {
    fn name(&self) -> &'static str {
        self.name
    }
    fn usage(&self) -> &'static str {
        self.name
    }
    fn describe(&self) -> &'static str {
        self.descr.unwrap_or("Get a specific image")
    }
    fn process(&self, context: &CmdContext, state: Option<&CmdState>) -> CmdResult {
        let mut result = if let Some(state) = state {
            let res_id = match state {
                CmdState::ResourceMap(_) => unreachable!(),
                CmdState::ResourceID(res_id) => res_id,
            };
            CmdResult::img(TelegramID(res_id.clone()))
        } else {
            CmdResult::img(Filename(self.filename.clone()))
        };
        if self.reply {
            if let Some(msg) = &context.msg.reply_to_message {
                result = result.reply_to(msg.message_id);
            } else {
                result = result.reply(true);
            }
        }
        result
    }
    fn process_result(
        &self,
        context: &CmdContext,
        _prev_result: &ResultData,
        state: Option<&CmdState>,
    ) -> Option<CmdState> {
        if state.is_some() {
            None
        } else {
            Some(CmdState::ResourceID(
                context.msg.photo.as_ref()?.first()?.file_id.clone(),
            ))
        }
    }
}
